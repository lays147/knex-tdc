
exports.up = function(knex) {
  return knex('badges').insert([
      { participant_id: 1, edition: 5, track: 'Javascript'},
      { participant_id: 2, edition: 5, track: 'Customer Success'},
      { participant_id: 3, edition: 6, track: 'Stadium'},
      { participant_id: 4, edition: 6, track: 'Stadium'},
      { participant_id: 5, edition: 2, track: 'Python'},
      { participant_id: 6, edition: 5, track: 'Microservices'},
  ])
};

exports.down = function(knex) {
  return knex('badges').del()
};
