
exports.up = function(knex) {
  return knex.schema.createTable('badges', (table) => {
      table.increments('id')
      table.timestamp('created_at').defaultTo(knex.fn.now())
      table.string('edition').notNullable()
      // table.uuid('participant_id')
      table.integer('participant_id')
      table.string('track').notNullable()

      table.foreign('participant_id').references('participants.id')
      table.unique(['participant_id', 'edition', 'track' ])
  })
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('badges')
};
