require('dotenv').config()

const DATABASE_URL=process.env.DATABASE_URL;
const DATABASE_USER=process.env.DATABASE_USER;
const DATABASE_PASSWORD=process.env.DATABASE_PASSWORD;
const DATABASE_NAME=process.env.DATABASE_NAME;

module.exports = {
  client: 'postgresql',
    connection: {
      host:     DATABASE_URL,
      database: DATABASE_NAME,
      user:     DATABASE_USER,
      password: DATABASE_PASSWORD
  }
};
