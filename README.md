## Sem ORM: Migração de banco com Javascript

### Comandos

#### Instalando o KnexJs

```sh
npm install -s knex # lib
npm install -g knex # cli
```

#### Criando o arquivo de configuração

```sh
knex init
```

### **Roteiro**

[Doc Knex](knexjs.org/)

1) Configurando o knexfile.js para ler variáveis de ambiente

2) Uma nova migração:  
`knex migrate:make name_your_migration`

3) Aplicando uma migração:  
`knex migrate:up migration_name.js`

4) Fazendo um rollback:  
`knex migrate:down migration_name.js`

**Lotes de migrações**:  
1) Aplicando todas as migrações pendentes em um lote:  
`knex migrate:latest`

2) Fazendo rollback de todas as migrações do último lote:  
`knex migrate:rollback`

3) Tabela migrations  
`select * from knex_migrations;`

### SQL

```sql
select * from participants;

select * from editions;

select * from badges;

select name, track, city, year from badges
    left join editions e on badges.edition = e.id
    left join participants p on badges.participant_id = p.id
    order by year;
```