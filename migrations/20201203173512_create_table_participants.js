
exports.up = function(knex) {
    return knex.schema.createTable('participants', (table) => {
        // table.uuid('id').defaultTo(knex.raw('uuid_generate_v4()')).unique()
        table.increments()
        table.string('name').notNullable()
        table.timestamps(true, true) // created_at | updated_at
    })
  };
  
  exports.down = function(knex) {
    return knex.schema.dropTableIfExists('participants')
  };
  