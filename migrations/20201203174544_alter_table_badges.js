
exports.up = function(knex) {
    return knex.schema.alterTable('badges', (table) =>  {
        table.integer('edition').alter()
        table.foreign('edition').references('editions.id')
    })
};

exports.down = function(knex) {
    return knex.schema.alterTable('badges', (table) =>  {
        table.string('edition').alter()
        table.dropForeign('edition')
    })
};
