
exports.up = function(knex) {
    return knex('participants').insert([
        {id: 1, name: 'Lays Rodrigues'},
        {id: 2, name: 'Yara Mascarenhas'},
        {id: 3, name: 'Bruno Souza'},
        {id: 4, name: 'Julio Mattos'},
        {id: 5, name: 'Flavio Pimenta'},
        {id: 6, name: 'Morvana Bonin'},
    ])
};

exports.down = function(knex) {
    return knex('participants').del()
};
