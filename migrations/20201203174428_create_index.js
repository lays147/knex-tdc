
exports.up = function(knex) {
    return knex.schema.alterTable('badges', (table) =>  {
        table.index('edition')
    })
};

exports.down = function(knex) {
    return knex.schema.alterTable('badges', (table) =>  {
        table.dropIndex('edition')
    })
};
