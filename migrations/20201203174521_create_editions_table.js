
exports.up = function(knex) {
    return knex.schema.createTable('editions', (table) =>  {
        table.increments()
        table.string('city').notNullable()
        table.string('year', 4).notNullable()
    }).then(() => {
        return knex('editions').insert([
            {id: 1, city: 'Belo Horizonte', year: '2020'},
            {id: 2, city: 'Florianópolis', year: '2020'},
            {id: 3, city: 'São Paulo', year: '2020'},
            {id: 4, city: 'Recife', year: '2020'},
            {id: 5, city: 'Porto Alegre', year: '2020'},
            {id: 6, city: 'Rio de Janeiro', year: '2021'},
        ])
    })
};

exports.down = function(knex) {
    return knex.schema.dropTableIfExists('editions')
};
